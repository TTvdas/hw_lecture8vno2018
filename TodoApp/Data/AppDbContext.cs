﻿using Microsoft.EntityFrameworkCore;
using TodoApp.Data.Entities;

namespace TodoApp.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        { }

        public AppDbContext()
        {   
        }

        //TODO: Take connection string from config. This is workaround to have parametreless DBContext constructor.
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseSqlServer("Data Source=localhost\\SQLEXPRESS;Initial Catalog=todo-app-db-8-lecture;Trusted_Connection=True;");

        public DbSet<TodoItem> TodoItems { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Priority> Priorities { get; set; }
        public DbSet<TodoItemDetails> TodoItemDetails { get; set; }
        public DbSet<Category> Categories { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<TodoItem>()
            //    .HasOne(a => a.Details)
            //    .WithOne(b => b.TodoItem)
            //    .HasForeignKey<TodoItemDetails>(b => b.TodoItemId);

            //modelBuilder.Entity<User>()
            //    .HasMany(u => u.TodoItems)
            //    .WithOne(t => t.User);

            //modelBuilder.Entity<User>()
            //    .HasMany(c => c.TodoItems)
            //    .WithOne(e => e.User)
            //    .IsRequired();

            modelBuilder.Entity<TodoItemCategory>()
                .HasKey(tc => new { tc.TodoItemId, tc.CategoryId });

            modelBuilder.Entity<TodoItemCategory>()
                .HasOne(bc => bc.TodoItem)
                .WithMany(b => b.TodoItemCategories)
                .HasForeignKey(bc => bc.TodoItemId);

            modelBuilder.Entity<TodoItemCategory>()
                .HasOne(tc => tc.Category)
                .WithMany(c => c.TodoItemCategories)
                .HasForeignKey(tc => tc.CategoryId);
        }

    }


}
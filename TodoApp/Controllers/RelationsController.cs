﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.EntityFrameworkCore;
using TodoApp.Data;
using TodoApp.Data.Entities;
using TodoApp.DataContracts;
using TodoApp.DataContracts.Requests;
using TodoApp.DataContracts.Responses;
using System.Data.SqlClient;

namespace TodoApp.Controllers
{
    [Route("api/relations")]
    public class RelationsController : Controller
    {

        public RelationsController()
        {
        }

        //[HttpPost("getUsers")]
        //public IEnumerable<User> getUsers()
        //{
        //    IEnumerable<User> users;
        //    using (AppDbContext db = new AppDbContext())
        //    {
        //        users = db.Users.ToList();
        //    }

        //    return users;
        //}

        [HttpPost("assignTaskForUser")]
        public void AssignTaskForUser([FromQuery] int userId, [FromQuery] string title, [FromQuery] bool completed)
        {
            
            using (AppDbContext db = new AppDbContext())
            {
                //Create new todoitem
                var newTodoItem = new TodoItem();
                newTodoItem.Title = title;
                newTodoItem.Completed = completed;

                //Find user
                User user = db.Users.Find(userId);

                //Detached scenarion, when only one call to database is done
                //User user = new User();
                //user.Id = userId;

                //Foreign key scenario
                //newTodoItem.UserId = userId;
                //db.Add(newTodoItem);

                if (user.TodoItems == null)
                {
                    user.TodoItems = new List<TodoItem>();
                }

                //assign task to user
                user.TodoItems.Add(newTodoItem);

                db.SaveChanges();
            }
        }

        [HttpPut("reassignTask")]
        public void ReassignTask([FromQuery] int userId, [FromQuery] int taskId)
        {
            using (AppDbContext db = new AppDbContext())
            {
                //Find user
                User newUser = db.Users.Find(userId);

                //Find todoitem
                TodoItem todoItem = db.TodoItems.Single(t => t.Id == taskId);
                
                if (newUser.TodoItems == null)
                {
                    newUser.TodoItems = new List<TodoItem>();
                }
                //re-assign task to user
                newUser.TodoItems.Add(todoItem);
                db.SaveChanges();


                //--------------------------------------
                //fakes approach
                //var todoItem = new TodoItem {Id = taskId};
                //db.Attach(todoItem);

                //var user = new User {Id = userId};
                //db.Attach(user);

                //user.TodoItems.Add(todoItem);
                //db.SaveChanges();

                //--------------------------------------
                //Altering foreign key approach
                //var todoItem = new TodoItem { Id = taskId };
                //db.Attach(todoItem);

                //todoItem.UserId = userId;

                //db.SaveChanges();
            }
        }

        [HttpDelete("unassignTask")]
        public void UnassignTask([FromQuery] int userId, [FromQuery] int taskId)
        {
            using (AppDbContext db = new AppDbContext())
            {
                //Find user
                User newUser = db.Users.Find(userId);

                //Find todoitem
                TodoItem todoItem = db.TodoItems.Find(taskId);

                
                //delete task to user
                newUser.TodoItems.Remove(todoItem);
                
                db.SaveChanges();
            }
        }

        [HttpPost("assignDescription")]
        public IActionResult AssignDescription(int? taskId, string descriptionText)
        {
            using(AppDbContext _context = new AppDbContext())
            {
                if (!taskId.HasValue)
                {
                    return BadRequest("Please, specify the task id value");
                }

                var todoItem = _context.TodoItems.Find(taskId);

                if(todoItem == null)
                {
                    return BadRequest($"No todo item with id: {taskId} was found");
                }

                if (string.IsNullOrEmpty(descriptionText))
                {
                    return BadRequest("Description cannot be null");
                }

                if(todoItem.Details == null)
                {
                    todoItem.Details = new TodoItemDetails();
                }

                todoItem.Details.TodoItemId = todoItem.Id;
                todoItem.Details.TodoItem = todoItem;
                todoItem.Details.Description = descriptionText;
                
                try // Other todo item details can reference this todo item
                {
                    _context.SaveChanges();
                }
                catch(Exception)
                {
                    return BadRequest("A todo item can only have one description");
                }

                return Ok();
            }
        }
    }
}

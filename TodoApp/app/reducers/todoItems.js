export default (state = [], action) => {
    switch (action.type) {
        case 'LOAD_TODOS':
          return action.appTodos;
        case 'ADD_TODO_SUCCESS':
            return [
                ...state, {
                    id: action.todo.id,
                    title: action.todo.title,
                    completed: action.todo.completed
                }];
        case 'REMOVE_TODO_SUCCESS':
            return state.filter(item => item.id !== action.id)
        case 'UPDATE_TODO_SUCCESS':
            return state.map(item => (item.id === action.todoItem.id ? action.todoItem : item))
        default:
            return state;
    }
};

﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TodoApp.Data;
using TodoApp.Data.Entities;
using TodoApp.DataContracts;
using TodoApp.DataContracts.Requests;
using TodoApp.DataContracts.Responses;

namespace TodoApp.Controllers
{
    [Route("api/reports")]
    public class ReportsController : Controller
    {
        private readonly AppDbContext _dbContext;
        public ReportsController(AppDbContext appDbContext)
        {
            _dbContext = appDbContext;
        }

        [HttpGet("getTodos")]
        public List<TodoItem> GetTodos()
        {
            return _dbContext.TodoItems
                                .ToList();
        }


        //[HttpGet("getTodosWithNotes")]
        //public object getTodosWithNotes()
        //{
        //    var todosItems = _dbContext.TodoItems
        //        .Include(t => t.TodoItemNote)
        //        .ToList();

        //    return
        //        todosItems
        //        .Select(
        //            t => new
        //            {
        //                TodoItemId = t.Id,
        //                TodoTitle = t.Title,
        //                NoteId = t?.TodoItemNote.Id,
        //                Note = t?.TodoItemNote.Note
        //            });
        //}

        [HttpGet("getUsersWithTasks")]
        public object GetUsersWithTasks()
        {
            var query = from user in _dbContext.Users
                        join todoItem in _dbContext.TodoItems
                            on user.Id equals todoItem.UserId
                        //where todoItem.Completed == true
                        select new
                        {
                            UserId = user.Id,
                            Name = user.Name,
                            TodoItemId = todoItem.Id,
                            TodoTitle = todoItem.Title,
                            IsComplete = todoItem.Completed
                        };

            var result = query.ToList();

            return result;
        }

        [HttpGet("getAllUsersWithTasks")]
        public object GetAllUsersWithTasks()
        {
            var query = from user in _dbContext.Users
                        join todoItem in _dbContext.TodoItems
                            on user.Id equals todoItem.UserId
                            into MatchedTodoItems                                                               //
                        from matchedTodoItem in MatchedTodoItems.DefaultIfEmpty()                               //
                        select new
                        {
                            UserId = user.Id,
                            Name = user.Name,
                            TodoItemId = matchedTodoItem != null ? matchedTodoItem.Id.ToString() : "NULL",      //
                            TodoTitle = matchedTodoItem != null ? matchedTodoItem.Title : "NULL",               //
                            IsComplete = matchedTodoItem != null ? matchedTodoItem.Completed.ToString() : "NULL",//
                        };

            var result = query.ToList();

            return result;
        }

        [HttpGet("getAllUsersWithTasksCount")]
        public object GetAllUsersWithTasksCount()
        {
            var query = from user in _dbContext.Users
                        let cCount = user.TodoItems.Count()
                        select new { Name = user.Name, Count = cCount };

            var result = query.ToList();

            return result;
        }

        [HttpGet("getAllTodoItemsWithDetails")]
        public object GetAllTodoItemsWithDetails()
        {
            var query = from todoItem in _dbContext.TodoItems
                        join details in _dbContext.TodoItemDetails
                        on todoItem.Id equals details.Id

                        select new
                        {
                            todoItem.Id,
                            todoItem.Title,
                            details.Description,
                        };

            return query.ToList();
        }

        [HttpGet("GetCountOfTodoItemsByPriority")]
        public object GetCountOfTodoItemsByPriority()
        {
            var query = from todoItem in _dbContext.TodoItems
                        join priority in _dbContext.Priorities
                        on todoItem.PriorityId equals priority.Id
                        into MatchedPriorities

                        from machedPriority in MatchedPriorities.DefaultIfEmpty()
                        group machedPriority by machedPriority.Name into groupedPriorities
                        select new
                        {
                            Priority = groupedPriorities.Key,
                            CountOfTodoItems = groupedPriorities.Count(),
                        };

            return query.ToList();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TodoApp.Data.Entities
{
    //DEPENDENT ENTITY
    public class TodoItemDetails
    {
        public int Id { get; set; }
        public string Description { get; set; }

        public int TodoItemId { get; set; }
        public TodoItem TodoItem { get; set; } //<<==NAVIGATION PROPERTY
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace TodoApp.Migrations
{
    public partial class InitialValuesForPriorities : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
                @"INSERT INTO[dbo].[Priorities]([Id], [Name]) VALUES(1, 'Low')
                INSERT INTO[dbo].[Priorities]([Id], [Name]) VALUES(2, 'Normal')
                INSERT INTO[dbo].[Priorities]([Id], [Name]) VALUES(3, 'High')"
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
                @"DELETE FROM Priorities WHERE Id=1
                DELETE FROM Priorities WHERE Id=2
                DELETE FROM Priorities WHERE Id=3"
            );
        }
    }
}

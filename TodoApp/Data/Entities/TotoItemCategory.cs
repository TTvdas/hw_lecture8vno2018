﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TodoApp.Data.Entities
{
    public class TodoItemCategory
    {
        public int TodoItemId { get; set; }
        public TodoItem TodoItem { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }
    }
}

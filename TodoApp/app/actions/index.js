import storageService from '../services/storageService';
import { api } from '../utils';

export const getAppTodos = () => (dispatch) => {
    api
      .get('/api/todos')
      .then(response => {
         dispatch({
           type: 'LOAD_TODOS',
           appTodos: response.data
         })
      });
};

export const addTodo = (title) => (dispatch) => {
  api
    .post('/api/todos', { title })
    .then((response) => {
      dispatch({
        type: 'ADD_TODO_SUCCESS',
        todo: response.data
      });
    });
};

export const removeTodo = id => (dispatch) => {
  api
    .delete(`/api/todos/${id}`)
    .then((response) => {
      if (!response.ok) return;
      dispatch({
        type: 'REMOVE_TODO_SUCCESS',
        id
      });
    });
};

export const updateTodo = todoItem => (dispatch) => {
  const { id, ...body } = todoItem;

  api
    .put(`/api/todos/${id}`, body)
    .then(() => {
      dispatch({
        type: 'UPDATE_TODO_SUCCESS',
        todoItem
      });
    });
}

export const changeColor = colorHexCode => (dispatch, getState) => {
  const currentSettings = getState().appSettings;

  storageService
    .updateAppSettings({ ...currentSettings, loading: false, appColor: colorHexCode })
    .then((response) => {
      dispatch({ type: 'CHANGE_COLOR_SUCCESS', appColor: response.appColor });
    });
};
﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace TodoApp.Migrations
{
    public partial class Added_InitialTodoItems : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                SET IDENTITY_INSERT dbo.TodoItems ON;  
                GO  
                
                INSERT INTO [dbo].[TodoItems] (Id, Completed, Title, UserId) VALUES(1, 0, 'task1', 1)
                INSERT INTO [dbo].[TodoItems] (Id, Completed, Title, UserId) VALUES(2, 0, 'task2', 1)
                INSERT INTO [dbo].[TodoItems] (Id, Completed, Title, UserId) VALUES(3, 1, 'task3', 2)
                INSERT INTO [dbo].[TodoItems] (Id, Completed, Title, UserId) VALUES(4, 1, 'task4', 2)
                INSERT INTO [dbo].[TodoItems] (Id, Completed, Title, UserId) VALUES(5, 1, 'task5', 3)
                INSERT INTO [dbo].[TodoItems] (Id, Completed, Title, UserId) VALUES(6, 0, 'task6', 3)
                INSERT INTO [dbo].[TodoItems] (Id, Completed, Title, UserId) VALUES(7, 0, 'task7', NULL)
                INSERT INTO [dbo].[TodoItems] (Id, Completed, Title, UserId) VALUES(8, 0, 'task8', 3)
                INSERT INTO [dbo].[TodoItems] (Id, Completed, Title, UserId) VALUES(9, 0, 'task9', NULL)
                INSERT INTO [dbo].[TodoItems] (Id, Completed, Title, UserId) VALUES(10, 1, 'task10', NULL)

                SET IDENTITY_INSERT dbo.TodoItems OFF;  
                GO");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE FROM TodoItems WHERE Id IN (1,2,3,4,5,6,7,8,9,10)                
            ");
        }
    }
}

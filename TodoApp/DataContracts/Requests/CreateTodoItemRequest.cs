﻿using System.ComponentModel.DataAnnotations;

namespace TodoApp.DataContracts.Requests
{
    public class CreateTodoItemRequest
    {
        [Required]
        public string Title { get; set; }
    }
}

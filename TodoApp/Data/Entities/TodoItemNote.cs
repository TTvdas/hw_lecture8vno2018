﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TodoApp.Data.Entities
{
    public class TodoItemNote
    {
        public int Id { get; set; }
        public string Note { get; set; }

        public TodoItem TodoItem { get; set; }
        public int TodoItemId { get; set; }
    
    }
}

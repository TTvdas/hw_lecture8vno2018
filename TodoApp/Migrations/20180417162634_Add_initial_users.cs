﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace TodoApp.Migrations
{
    public partial class Add_initial_users : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                SET IDENTITY_INSERT dbo.Users ON;  
                GO  
                
                INSERT INTO [dbo].[Users] (Id, [Age],[IsAdmin],[Name]) VALUES (1, 21, 1, 'Jonas')
                INSERT INTO [dbo].[Users] (Id, [Age],[IsAdmin],[Name]) VALUES (2, 35, 0, 'Egle')
                INSERT INTO [dbo].[Users] (Id, [Age],[IsAdmin],[Name]) VALUES (3, 45, 0, 'Petras')
                INSERT INTO [dbo].[Users] (Id, [Age],[IsAdmin],[Name]) VALUES (4, 8, 0, 'Ona')
                INSERT INTO [dbo].[Users] (Id, [Age],[IsAdmin],[Name]) VALUES (5, 24, 0, 'Kazys')

                SET IDENTITY_INSERT dbo.Users OFF;  
                GO");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE FROM Users WHERE Name = 'Jonas'
                DELETE FROM Users WHERE Name = 'Egle'
                DELETE FROM Users WHERE Name = 'Petras'
                DELETE FROM Users WHERE Name = 'Ona'
            ");
        }
    }
}

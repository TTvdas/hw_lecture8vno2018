﻿using System.Collections.Generic;

namespace TodoApp.Data.Entities
{
    public class TodoItem
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public bool Completed { get; set; }

        public TodoItemDetails Details { get; set; }

        public User User { get; set; } 
        public int? UserId { get; set; } 

        public ICollection<TodoItemCategory> TodoItemCategories { get; set; }

        public int PriorityId { get; set; }
        public Priority Priority { get; set; }
    }
}

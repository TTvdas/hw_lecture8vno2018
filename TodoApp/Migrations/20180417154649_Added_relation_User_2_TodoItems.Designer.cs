﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Storage.Internal;
using System;
using TodoApp.Data;

namespace TodoApp.Migrations
{
    [DbContext(typeof(AppDbContext))]
    [Migration("20180417154649_Added_relation_User_2_TodoItems")]
    partial class Added_relation_User_2_TodoItems
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.0.2-rtm-10011")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("TodoApp.Data.Entities.Priority", b =>
                {
                    b.Property<int>("Id");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Priorities");
                });

            modelBuilder.Entity("TodoApp.Data.Entities.TodoItem", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Completed");

                    b.Property<string>("Title");

                    b.Property<int?>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("TodoItems");
                });

            modelBuilder.Entity("TodoApp.Data.Entities.TodoItemDetails", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description");

                    b.Property<int>("TodoItemId");

                    b.HasKey("Id");

                    b.HasIndex("TodoItemId")
                        .IsUnique();

                    b.ToTable("TodoItemDetails");
                });

            modelBuilder.Entity("TodoApp.Data.Entities.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Age");

                    b.Property<bool?>("IsAdmin");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("TodoApp.Data.Entities.TodoItem", b =>
                {
                    b.HasOne("TodoApp.Data.Entities.User")
                        .WithMany("TodoItems")
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("TodoApp.Data.Entities.TodoItemDetails", b =>
                {
                    b.HasOne("TodoApp.Data.Entities.TodoItem", "TodoItem")
                        .WithOne("Details")
                        .HasForeignKey("TodoApp.Data.Entities.TodoItemDetails", "TodoItemId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}

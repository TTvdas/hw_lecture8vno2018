﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace TodoApp.Migrations
{
    public partial class Added_1to1_TodoItem_to_TodoItemDetails : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TodoItemId",
                table: "TodoItemDetails",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_TodoItemDetails_TodoItemId",
                table: "TodoItemDetails",
                column: "TodoItemId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_TodoItemDetails_TodoItems_TodoItemId",
                table: "TodoItemDetails",
                column: "TodoItemId",
                principalTable: "TodoItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TodoItemDetails_TodoItems_TodoItemId",
                table: "TodoItemDetails");

            migrationBuilder.DropIndex(
                name: "IX_TodoItemDetails_TodoItemId",
                table: "TodoItemDetails");

            migrationBuilder.DropColumn(
                name: "TodoItemId",
                table: "TodoItemDetails");
        }
    }
}

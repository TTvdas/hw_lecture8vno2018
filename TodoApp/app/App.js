import { hot } from 'react-hot-loader';
import React from 'react';
import { Router, Route, IndexRoute } from 'react-router';
import { Provider } from 'react-redux';

import css from '../public/styles.css';
import TodoApp from './TodoApp';
import TodoAppTaskList from './TodoAppTaskList';
import TodoAppSettings from './TodoAppSettings';
import store, {history} from './store';

const App = () => (
    <Provider store={store}>
        <Router history={history}>
            <Route path="/" component={TodoApp}>
                <IndexRoute component={TodoAppTaskList} />
                <Route path="settings" component={TodoAppSettings} />
            </Route>
        </Router>
    </Provider>
);

export default hot(module)(App);